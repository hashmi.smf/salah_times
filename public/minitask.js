$(document).ready(function(){
    var dates = ["Date","Salah Timings"];

    var timings = [
     {"Date":"15-05-2021","Salah Timings":{"Fajr":"04:00","Duhr":"13:00","Asr":"17:00","Magrib":"19:00","Isha":"20:00"}},
    {"Date":"16-05-2021","Salah Timings":{"Fajr":"04:00","Duhr":"13:00","Asr":"17:00","Magrib":"19:00","Isha":"20:00"}},
    {"Date":"17-05-2021","Salah Timings":{"Fajr":"04:00","Duhr":"13:00","Asr":"17:20","Magrib":"19:00","Isha":"19:20"}},
    {"Date":"18-05-2021","Salah Timings":{"Fajr":"04:00","Duhr":"13:20","Asr":"17:00","Magrib":"19:00","Isha":"19:45"}},
    {"Date":"19-05-2021","Salah Timings":{"Fajr":"04:00","Duhr":"13:00","Asr":"17:22","Magrib":"19:20","Isha":"20:00"}},
    {"Date":"20-05-2021","Salah Timings":{"Fajr":"04:21","Duhr":"13:00","Asr":"17:22","Magrib":"12:09","Isha":"20:09"}},
    {"Date":"21-05-2021","Salah Timings":{"Fajr":"04:21","Duhr":"13:00","Asr":"17:02","Magrib":"12:09","Isha":"20:20"}},
    {"Date":"22-05-2021","Salah Timings":{"Fajr":"04:21","Duhr":"13:00","Asr":"17:02","Magrib":"12:05","Isha":"20:20"}},
    {"Date":"23-05-2021","Salah Timings":{"Fajr":"04:22","Duhr":"13:00","Asr":"17:02","Magrib":"17:05","Isha":"20:20"}},
    {"Date":"24-05-2021","Salah Timings":{"Fajr":"04:22","Duhr":"13:00","Asr":"17:02","Magrib":"17:05","Isha":"20:20"}},
    {"Date":"25-05-2021","Salah Timings":{"Fajr":"04:00","Duhr":"13:00","Asr":"17:00","Magrib":"17:05","Isha":"20:20"}},
    {"Date":"26-05-2021","Salah Timings":{"Fajr":"04:00","Duhr":"13:00","Asr":"17:02","Magrib":"17:05","Isha":"20:20"}},
    {"Date":"27-05-2021","Salah Timings":{"Fajr":"04:00","Duhr":"13:00","Asr":"17:02","Magrib":"17:05","Isha":"20:20"}},
    {"Date":"28-05-2021","Salah Timings":{"Fajr":"04:00","Duhr":"13:00","Asr":"17:02","Magrib":"17:05","Isha":"20:20"}},
    {"Date":"29-05-2021","Salah Timings":{"Fajr":"04:00","Duhr":"13:00","Asr":"17:02","Magrib":"17:05","Isha":"20:20"}},
    {"Date":"30-05-2021","Salah Timings":{"Fajr":"04:00","Duhr":"13:00","Asr":"17:02","Magrib":"17:05","Isha":"20:20"}},
    {"Date":"31-05-2021","Salah Timings":{"Fajr":"04:00","Duhr":"13:00","Asr":"17:02","Magrib":"17:05","Isha":"20:20"}},
];
     

    $(dates).each(function(index,value){
        var content = `<th>${value}</th>`;
        $("table thead tr").append(content);
    });

    $(timings).each(function(index,object){
        var content = `<tr>
         <td>${object.Date}</td>
        <td>
        <ul>
            <li>
            FAJR:${object["Salah Timings"].Fajr}
            </li>
            <li>
            DUHR:${object["Salah Timings"].Duhr}
            </li>
            <li>
            ASR:${object["Salah Timings"].Asr}
            </li>
            <li>
            MAGRIB:${object["Salah Timings"].Magrib}
            </li>
            <li>
            ISHA:${object["Salah Timings"].Isha}
            </li>
        </ul>
        </td>
    </tr>`;
        $("tbody").append(content);
    });

});