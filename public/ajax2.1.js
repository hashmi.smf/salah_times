$(document).ready(function() {
        // comments method
    function fetchComments() {
        return $.ajax({
            url: 'https://jsonplaceholder.typicode.com/comments',
            type: 'get',
            dataType: 'json'
        });
    }
        // users method
    function fetchUsers() {
        return $.ajax({
            url: 'https://jsonplaceholder.typicode.com/users',
            type: 'get',
            dataType: 'json'
        });
    }
        // post Method
    function fetchPosts() {
        return $.ajax({
            url: 'https://jsonplaceholder.typicode.com/posts',
            type: 'get',
            dataType: 'json'
        });
    }
        // function
    $.when(fetchComments(), fetchUsers(), fetchPosts()).then(function(fetchComments, fetchUsers, fetchPosts) {
        var comments = fetchComments[0];
        var users = fetchUsers[0];
        var posts = fetchPosts[0];
        var content = '';
        $(posts).each(function(index, post) {
            content += `<div class="card border-primary" style="margin-bottom:30px;">
                      <div class="card-header">PostId:${post.id}</div>
                      <div class="card-body">
                        <h4 class="card-title">${post.title}</h4>
                        <p class="card-text">${post.body}</p>
                        <p>
                        <button class="btn btn-primary commentButton" type="button" data-toggle="collapse"
                        data-target="#collapse${index}" aria-expanded="false" aria-controls="collapse${index}">More Comments
                        </button>`

                        $(users).each(function(index, user) {
                            if (user.id == post.userId) {
                                content += `<h5 class = "float-right">- ${user.name}</h5>`;
                            }
                        });
                        
                         `</p>
                          <div class "row">
                          <div class "col">
                          <div>`;


            content += `<div class="commentsList card-body collapse" id="collapse${index}">`;
            $(comments).each(function(index, comment) {
                if (post.id == comment.postId) {
                    content += `<p class = " m-3 p-2 border border-primary ">${comment.body}</p>`;
                }
            });
            content += `</div></div></div></div></div></div>`;
        });
        $(".allPosts").html(content);
    });
        //  onclick function
    $(document).on('click', '.commentButton', function(e) {
        var a = $(this).attr("data-target");
        $(a).addClass("show");
        $(".commentsList").removeClass("show");
        if ($(e.target).text().trim() === 'More Comments') {
            $(e.target).text('Hide Comments!');
        } else {
            $(e.target).text('More Comments');
        }
    });
});