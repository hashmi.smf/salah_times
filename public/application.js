if (5 == 5) {
  $("#monthlyTimings").addClass('table-hover');
} else {
  // console.log('table-hover');
}


$(document).ready(function() {
  var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
  var tHead = ["Date","Ramadan","Fajr","Zuhr","Asr","Magrib","Isha"];
  var Dtime = ["Fajr","zuhr","Asr", "Magrib","Isha"];
  var specTimeHeader = ["Salah", "Time", "Iqamah"];
  var specTimeBody =
  [
    {"Salah": "Fajr", "Time": "04:30", "Iqamah": "04:45"},
    {"Salah": "Duhr", "Time": "13:00", "Iqamah": "13:30"},
    {"Salah": "Asr", "Time": "16:00", "Iqamah": "16:15"},
    {"Salah": "Magrib", "Time": "18:42", "Iqamah": "18:42"},
    {"Salah": "Isha", "Time": "19:45", "Iqamah": "20:00"}
  ];


  $(specTimeHeader).each(function(index, value){
    var content = `<th scope="col">${value}</th>`;
    $(".specTimeHeader tr").append(content);
  });

  $(specTimeBody).each(function(index, object){
    var content = `<tr>
      <td>${object.Salah}</td>
      <td>${object.Time}</td>
      <td>${object.Iqamah}</td>
     </tr>`
     $(".specTimeBody").append(content);
  });



  $(months).each(function(index,value) {
    var content = `<a class="dropdown-item" href="#">${value}</a>`;
      $(".dropdown-menu").append(content);
  });

  $(tHead).each(function(index, value) {
    var content = `<th scope="col" class="text">${value}</th>`;
      $(".thead-dark tr").append(content);
  });


  // $(Dtime).each(function(index,value){
  //   var content =`<th scope="row">${value}</th>`;
  //   $(".dtime").append(content);
  // });

  var tableData = [
    {
      "date": "01/01/2021",
      "Ramadan": 01,
      "Fajr": "04:45",
      "Zuhr": "12:30",
      "Asr": "16:30",
      "Magrib": "18:00",
      "Isha": "19:45"
    },

    {"date": "02/01/2021", "Ramadan": 02, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "03/01/2021", "Ramadan": 03, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "04/01/2021", "Ramadan": 04, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "05/01/2021", "Ramadan": 05, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "06/01/2021", "Ramadan": 06, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "07/01/2021", "Ramadan": 07, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "08/01/2021", "Ramadan": 08, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "09/01/2021", "Ramadan": 09, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "10/01/2021", "Ramadan": 10, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "11/01/2021", "Ramadan": 11, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "12/01/2021", "Ramadan": 12, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "12/01/2021", "Ramadan": 12, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "14/01/2021", "Ramadan": 14, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "15/01/2021", "Ramadan": 15, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "16/01/2021", "Ramadan": 16, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "17/01/2021", "Ramadan": 17, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "18/01/2021", "Ramadan": 18, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "19/01/2021", "Ramadan": 19, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "20/01/2021", "Ramadan": 20, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "21/01/2021", "Ramadan": 21, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "22/01/2021", "Ramadan": 22, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "23/01/2021", "Ramadan": 23, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "24/01/2021", "Ramadan": 24, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "25/01/2021", "Ramadan": 25, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "26/01/2021", "Ramadan": 26, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "27/01/2021", "Ramadan": 27, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "28/01/2021", "Ramadan": 28, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "29/01/2021", "Ramadan": 29, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"},
    {"date": "30/01/2021", "Ramadan": 30, "Fajr": "04:45", "Zuhr": "12:30", "Asr": "16:30", "Magrib": "18:00", "Isha": "19:45"}
  ];



  $(tableData).each(function(index, object) {
    var content = `<tr>
          <td class="bg-light">${object.date}</td>
          <td>${object.Ramadan}</td>
          <td>${object.Fajr}</td>
          <td>${object.Zuhr}</td>
          <td>${object.Asr}</td>
          <td>${object.Magrib}</td>
          <td>${object.Isha}</td>
      </tr>`;
      $("#monthlyTimings tbody").append(content);
  });
  $('#dropdownMenuButton').click(function(){
    $('#monthlyTimings').fadeToggle();
  })
});
// $(function() {
//   $("#dropdownMenuButton").hide(300).show(1000);
// });





/*
Till now we have completed
1.if condition
2.array object and json
3.loops

array length
array index
array value should read like a[index number]
array is comma separated

*/


a = ['samir', 25, 'majeed', 26, 'hashmi', 29];
a.length = 6;
a[0];
a[1];
a[2];
a[3];
a[4];
a[5];

a = ['samir', 25, 'majeed', 26, 'hashmi', 29];

d = {"name": "samir", "age": 25, "qualification": "B.com"};

e = {"name": "Majeed", "age": 26, "qualification": "B.tech"};

k = [
  {"name": "samir", "age": 25, "qualification": "B.com"},
  {"name": "Majeed", "age": 26, "qualification": "B.tech"},
  {"name": "Hashmi", "age": 29, "qualification": "B.tech"}
]

namaz_timings = [{"date": "06-05-2021", "fajar": "5:00", "zohar": "13:00", "asar": "17:00", "magrib": "18:42", "isha": "20:00", "country": "India"},
{"date": "07-05-2021", "fajar": "5:10", "zohar": "13:00", "asar": "17:00", "magrib": "18:45", "isha": "20:00", "country": "India"},
{"date": "08-05-2021", "fajar": "5:10", "zohar": "13:00", "asar": "17:00", "magrib": "18:45", "isha": "20:00", "country": "India"},
{"date": "09-05-2021", "fajar": "5:10", "zohar": "13:00", "asar": "17:00", "magrib": "18:45", "isha": "20:00", "country": "India"},
{"date": "10-05-2021", "fajar": "5:09", "zohar": "13:00", "asar": "17:00", "magrib": "18:46", "isha": "20:00", "country": "India"}]


/*
object

*/
