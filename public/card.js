$(document).ready(function () {
  fetchTimings(5, 2021);
  var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

  var year = ["2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030"];
  // var countrylist = 
  //   {"canada":"{"latitude":"43.6534817", "longitude":"-79.3839347"}",
  //    '"india"', 'australia'};
  //   {"canada": {"latitude":"43.6534817", "longitude":"-79.3839347"}},
  //   {"india": {"latitude":"17.360589", "longitude":"78.4740613"}},
  //   {"australia": {"latitude":"-33.8548157", "longitude":"151.2164539"}}
  // ];
  // var country_attributes = {
  //   "canada": {"latitude":"43.6534817", "longitude":"-79.3839347"},
  //   "india": {"latitude":"17.360589", "longitude":"78.4740613"},
  //   "australia": {"latitude":"-33.8548157", "longitude":"151.2164539"}
  //                           };

// console.log(countrylist);
  $(months).each(function (index, value) {
    var content = `<option value = "${index+1}">${value}</option>`;
    $("#selectedmonth").append(content);
  });

  $(year).each(function (index, value) {
    var content = `<option value = "${value}">${value}</option>`;
    $("#selectedyear").append(content);
  });

  // $(countrylist).each(function (index, object) {
  //   var content = `<option value = "${object.canada.latitude + longitude} .">${value}</option>`;
  //   $("#selectedcountry").append(content);
  // });

  function fetchTimings(month, year) {
    // var canada = {"latitude":"43.6534817", "longitude":"-79.3839347"};
    // var india = {"latitude":"17.360589", "longitude":"78.4740613"};
    // console.log(country);
    // var australia {"latitude":"-33.8548157", "longitude":"151.2164539"};
    // var country_lat = country.latitude;
    // var country_long = country.longitude;
    $.ajax({
      // url: `http://api.aladhan.com/v1/calendar?latitude=${country_lat}&longitude=${country_long}&method=1&month=${month}&year=${year}`,
      url: `http://api.aladhan.com/v1/calendar?latitude=17.399808&longitude=78.3941632&method=1&month=${month}&year=${year}`,
      type: 'get',
      dataType: 'json',
      success: function (data) {
        var specTimeBody = data.data;
        
        var htmlData = '';
        $(specTimeBody).each(function (index, object) {
          var id = object.date.gregorian.month.en + object.date.gregorian.date + object.date.gregorian.year
          var content = `<div class="col-md-6" style="margin-bottom:15px">
          <div class="card">
            <div class="card-header bg-secondary" data-toggle="collapse" data-target="#${id}" aria-expanded="false" aria-controls="${id}">
              <h5 class="card-title">Date <span class="float-right">${object.date.readable}</span><span></a><i class="bi bi-chevron-down"></i></a></span></h5>
            </div>
            <div class="collapse" id="${id}">
            <div class="card card-body">
      
            <div class="card-body" style="padding:0">
              <ul class="list-group list-group-flush">
                <li class="list-group-item">Fajar <span class="float-right">${object.timings.Fajr}</span></li>
                <li class="list-group-item">Zohar <span class="float-right">${object.timings.Dhuhr}</span></li>
                <li class="list-group-item">Asr <span class="float-right">${object.timings.Asr}</span></li>
                <li class="list-group-item">Magrib <span class="float-right">${object.timings.Maghrib}</span></li>
                <li class="list-group-item">Isha <span class="float-right">${object.timings.Isha}</span></li>
              </ul>
              </div>
            </div>
            <div class="card-footer">
            <span class="float-left">Sunrise: ${object.timings.Sunrise}</span>
            <span class="float-right">Sunset: ${object.timings.Sunset}</span>
            </div>
            </div>
            </div>
        </div>`;
        
          htmlData += content;
        });
        $('.appendCard').html(htmlData);
      },
      error: function (error) {}
    });
  }

  $("#selectedmonth").change(function () {
    var month = $(this).val();
    var year = $('#selectedyear').val();
    // var country = $("#selectedcountry").val();
    fetchTimings(month, year);
  });

  $("#selectedyear").change(function () {
    var month = $("#selectedmonth").val();
    var year = $(this).val();
    // var country = $("#selectedcountry").val();
    fetchTimings(month, year);
  });

  // $("#selectedcountry").change(function () {
  //   var month = $("#selectedmonth").val();
  //   var year = $('#selectedyear').val();
  //   var country = $(this).val();
  //   fetchTimings(month, year, country);
  // });
});
