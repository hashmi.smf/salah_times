$(document).ready(function () {
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    $(months).each(function (index, object) {
        var content = `<option value = "${data[0].title}">${data[0].title}</option>`;
        $("#selectedmonth").append(content);
      });

$.ajax({
    url: `https://jsonplaceholder.typicode.com/posts`,
    type: 'get',
    dataType: 'json',
    success: function (data) {
      var specTimeBody = data;
      debugger
    //   var htmlData = '';
      $(specTimeBody).each(function (index, object) {
        // var id = object.date.gregorian.month.en + object.date.gregorian.date + object.date.gregorian.year
        var content = `<div class="col-md-6" style="margin-bottom:15px">
        <div class="card">
          <div class="card-header bg-secondary" data-toggle="collapse" data-target="#${data[0].title}" aria-expanded="false" aria-controls="${data[0].title}">
            <h5 class="card-title">Date <span class="float-right">${object.date.readable}</span><span></a><i class="bi bi-chevron-down"></i></a></span></h5>
          </div>
          <div class="collapse" id="${data[0].title}">
          <div class="card card-body">
    
          <div class="card-body" style="padding:0">
            <ul class="list-group list-group-flush">
              <li class="list-group-item">FAJR <span class="float-right">${data[0].title}</span></li>
              <li class="list-group-item">ZOHR <span class="float-right">${data[0].title}</span></li>
              <li class="list-group-item">ASR <span class="float-right">${object.timings.Asr}</span></li>
              <li class="list-group-item">MAGRIB <span class="float-right">${object.timings.Maghrib}</span></li>
              <li class="list-group-item">ISHA <span class="float-right">${object.timings.Isha}</span></li>
            </ul>
            </div>
          </div>
          <div class="card-footer">
          <span class="float-left">SUNRISE: ${object.timings.Sunrise}</span>
          <span class="float-right">SUNSET: ${object.timings.Sunset}</span>
          </div>
          </div>
          </div>
      </div>`;
      
        htmlData += content;
      });
      $('.appendCard').html(htmlData);
    },
    error: function (error) {}
  });
});
