$(document).ready(function() {
    // photos
    function fetchPhotos() {
        return $.ajax({
            url : "https://jsonplaceholder.typicode.com/photos",
            type : "get",
            dataType : "json"
        });
    }
    // albums
    function fetchAlbums() {
        return $.ajax({
            url : "https://jsonplaceholder.typicode.com/albums",
            type : "get",
            dataType : "json"
        });
    }
    // fetch condition
    $.when(fetchPhotos(),fetchAlbums()).then(function(fetchPhotos, fetchAlbums) {
        var photos = fetchPhotos[0];
        var albums = fetchAlbums[0];
        var content = '';
        $(albums).each(function(index, album) {
            content +=`<div class="card border-primary" style="margin-bottom:30px;">
            <div class="card-header">Album id: ${album.id}</div>
            <div class="card-body">
              <h4 class="card-title">${album.title}</h4>
              <p>
              <button class="btn btn-primary photoButton" type="button" data-toggle="collapse${album.id}"
              data-target="#collapse${album.id}" aria-expanded="false" aria-controls="collapse${album.id}">Photos
              </button>
              <div class="row">
              <div class="col">
              <div>
                  </p>`

                content += `<div class="photoList card-body collapse" id="collapse${album.id}">`;
                // photos loop
                $(photos).each(function(index, photo) {
                    if (photo.albumId == album.userId) {
                        content += `<img src="${photo.thumbnailUrl}"class="m-1 p-1 border rounded" width="100" height="100">`;
                    }
                });
                content += `</div></div></div></div></div></div>`;
            });
        $(".albums").html(content);
        });
        // onclick function
        $(document).on('click', '.photoButton', function(e) {
            var a = $(this).attr("data-target");
            $(a).addClass("show");
            $(".photoList").removeClass("show");
            if ($(e.target).text().trim() === 'Photos') {
                $(e.target).text('Hide photos!');
            } else {
                $(e.target).text('Photos');
            }
        });
        // end 
    });
