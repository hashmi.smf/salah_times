$('.confirm_appointment').submit(function(e) {
    e.preventDefault();
    var title = $("#title").val();
    var body = $("#body").val();
    var data = {title: title, body: body};

    $.ajax({
        url: 'https://jsonplaceholder.typicode.com/posts',
        data: data,
        type: 'post',
        dataType: 'json',
      success: function(response) {
        console.log(response);
      },
      error: function(error) {
        console.log(error);
      }
    });
});